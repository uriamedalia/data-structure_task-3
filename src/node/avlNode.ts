import { comparationResult } from "../tree/comperation.types";
import Node from "./node";

export const enum BalanceState {
    UNBALANCED_RIGHT = 2,
    SLIGHTLY_UNBALANCED_RIGHT = 1,
    BALANCED = 0,
    SLIGHTLY_UNBALANCED_LEFT = -1,
    UNBALANCED_LEFT = -2,
  }
export default class AvlNode<T> extends Node<T> {
  constructor(public key: T, public parent?: AvlNode<T>, public left?: AvlNode<T>, public right?: AvlNode<T>) {
    super(key, parent, left, right);
  }

  rotateLeft(): AvlNode<T> {
    //   a                              b
    //  / \                            / \
    // c   b   -> a.rotateLeft() ->   a   e
    //    / \                        / \
    //   d   e                      c   d

    const other = <AvlNode<T>>this.right;
    this.right = other.left;
    other.left = this;
    return other;
  }

  rotateRight(): AvlNode<T> {
    //     b                           a
    //    / \                         / \
    //   a   e -> b.rotateRight() -> c   b
    //  / \                             / \
    // c   d                           d   e

    const other = <AvlNode<T>>this.left;
    this.left = other.right;
    other.right = this;
    return other;
  }

  balanceFactor(): BalanceState {
    return this?.right.getHeight() || 0 - this?.left.getHeight() || 0;
  }
}
