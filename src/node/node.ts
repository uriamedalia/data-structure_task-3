import * as _ from "lodash";

interface INode<T> {
  key: T;
  left?: Node<T>;
  right?: Node<T>;
  parent?: Node<T>;
}

export default class Node<T> implements INode<T> {
  constructor(public key: T, public parent?: Node<T>, public left?: Node<T>, public right?: Node<T>) {}

  public get height(): number {
    return Math.max(this.left?.height || 0, this.right?.height || 0) + 1;
  }

  public getHeight(): number {
    return this._getHeight(this);
  }

  private _getHeight(node: Node<T>): number {
    if (!node) {
      return -1;
    }
    return Math.max(this._getHeight(node.left), this._getHeight(node.right)) + 1;
  }
}
