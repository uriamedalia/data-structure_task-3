import Point from "./point";
import AvlTree from "./tree/avlTree";
import SearchBinaryTree from "./tree/searchBinaryTree";
import Node from "./node/node";
import { ComparisonResult } from "./tree/comperation.types";

const TREE_SIZE = 1000;

const buildTree = (array: Point[]) => {
  const myTree = new SearchBinaryTree<Point>(insertComparePoints);
  array.forEach((point) => myTree.insert(new Node(point)));

  return myTree;
};

const nearestRightPoint = (tree: AvlTree<Point>, x0: number): Point => {
  return tree.search(new Point(x0, 0), compareNearestRightPoints).key || new Point(0, 0);
};

const getRandomPoints = (size: number): Point[] => {
  // TODO: make it real numbers by delete 'Math.round'
  return [...Array(size)].map((x) => {
    return new Point(Math.round(Math.random() * 100), Math.round(Math.random() * 100));
  });
};

const insertComparePoints = (a: Point, b: Point): ComparisonResult => {
  if (a.x > b.x) return ComparisonResult.GREATER;
  if (a.x < b.x) return ComparisonResult.SMALLER;
  return ComparisonResult.EQUAL;
};

const compareNearestRightPoints = (node: Point, line: Point): ComparisonResult => {
  if (node.x > line.x) return ComparisonResult.EQUAL;
  return ComparisonResult.SMALLER;
};

const app = () => {
  const myTree = buildTree(getRandomPoints(TREE_SIZE));

  console.log(myTree.getHeight());

  console.log(myTree.inorderWalk());
};

app();
