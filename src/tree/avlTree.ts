import SearchBinaryTree from "./searchBinaryTree";
import AvlNode from "../node/avlNode";

export default class AvlTree<T> extends SearchBinaryTree<T> {
  
  insert(nodeToInsert: AvlNode<T>): AvlNode<T> {
    super.insert(nodeToInsert);

    throw new Error("Method not implemented.");
  }

  delete(node: AvlNode<T>): AvlNode<T> {
    super.delete(node);

    throw new Error("Method not implemented.");
  }
}
