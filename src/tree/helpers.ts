import { comparationResult } from "./comperation.types";

export const defaultCompare = (a: any, b: any) : comparationResult => {
    if (a > b) {
      return 1;
    }
    if (a < b) {
      return -1;
    }
    return 0;
  }