export type CompareFunction<T> = (a: T, b: T) => comparationResult;
export type comparationResult = 1 | 0 | -1;

export enum ComparisonResult {
  EQUAL = 0,
  GREATER = 1,
  SMALLER = -1,
}
