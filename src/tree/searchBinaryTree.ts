import * as _ from "lodash";
import BaseTree from "./baseTree";
import Node from "../node/node";
import { ComparisonResult } from "./comperation.types"

export interface IBinaryTree<T> {
  minimum(x: Node<T>): Node<T>;
  maximum(x: Node<T>): Node<T>;
  predecessor(x: Node<T>): Node<T>;
  successor(x: Node<T>): Node<T>;
  insert(node: Node<T>): Node<T>;
  delete(node: Node<T>): Node<T>;
}

export default class SearchBinaryTree<T> extends BaseTree<T> implements IBinaryTree<T> {
  minimum(x: Node<T>): Node<T> {
    if (!x.left) {
      return x;
    }
    return this.minimum(x.left);
  }

  maximum(x: Node<T>): Node<T> {
    if (!x.right) {
      return x;
    }
    return this.maximum(x.right);
  }

  predecessor(x: Node<T>): Node<T> {
    throw new Error("Method not implemented.");
  }

  successor(x: Node<T>): Node<T> {
    if (!!x.right) {
      return this.minimum(x.right);
    }

    let current = x;
    let parent = x.parent;

    while (!!parent && current === parent.right) {
      current = parent;
      parent = parent.parent;
    }

    return parent;
  }

  insert(nodeToInsert: Node<T>): Node<T> {
    let currentNode = this.root;
    let parentOfCurrentNode;

    while (!!currentNode) {
      parentOfCurrentNode = currentNode;
      this.compare(nodeToInsert.key, currentNode.key) === ComparisonResult.GREATER
        ? (currentNode = currentNode.left)
        : (currentNode = currentNode.right);
    }

    nodeToInsert.parent = parentOfCurrentNode;

    if (!parentOfCurrentNode) {
      this.root = nodeToInsert;
    } else {
        this.compare(nodeToInsert.key, parentOfCurrentNode.key) === ComparisonResult.GREATER
        ? (parentOfCurrentNode.left = nodeToInsert)
        : (parentOfCurrentNode.right = nodeToInsert);
    }

    return nodeToInsert;
  }

  delete(node: Node<T>): Node<T> {
    let nodeToDelete = !(node.left && node.right) ? node : this.successor(node);

    let childToDeletedNode = !!nodeToDelete.left ? nodeToDelete.left : nodeToDelete.right;

    !!childToDeletedNode && (childToDeletedNode.parent = nodeToDelete.parent);

    if (!!nodeToDelete.parent) {
      this.root = childToDeletedNode;
    } else {
      nodeToDelete.parent.left === nodeToDelete
        ? (nodeToDelete.parent.left = childToDeletedNode)
        : (nodeToDelete.parent.right = childToDeletedNode);
    }

    nodeToDelete !== node && (node.key = nodeToDelete.key);

    return node;
  }
}
