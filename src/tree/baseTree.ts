import * as _ from "lodash";
import Node from "../node/node";
import { CompareFunction, ComparisonResult } from "./comperation.types";
import { defaultCompare } from "./helpers";

export default abstract class BaseTree<T> {
  protected root: Node<T>;
  protected compare: CompareFunction<T>;

  constructor(compare?: CompareFunction<T>, root?: Node<T>) {
    this.root = root;
    this.compare = compare || defaultCompare;
  }

  search(key: T, compare = this.compare): Node<T> {
    return this._search(key, this.root, compare);
  }

  preorderWalk(): T[] {
    return this._preorderWalk(this.root);
  }

  inorderWalk(): T[] {
    return this._inorderWalk(this.root);
  }

  postorderWalk(): T[] {
    return this._postorderWalk(this.root);
  }

  getHeight(): number {
    if (!this.root) {
      return -1;
    }
    return this.root.getHeight();
    // return node.height;
  }

  private _search(key: T, node: Node<T>, compare: CompareFunction<T>): Node<T> {
    if (!node) {
      return null;
    }
    switch (compare(node.key, key)) {
      case ComparisonResult.EQUAL:
        return node;

      case ComparisonResult.GREATER:
        return this._search(key, node.left, compare);

      case ComparisonResult.SMALLER:
        return this._search(key, node.right, compare);

      default:
        return null;
    }
  }

  private _preorderWalk(node: Node<T>): T[] {
    if (!node) {
      return [];
    }
    return [node.key, ...this._preorderWalk(node.left), ...this._preorderWalk(node.right)];
  }

  private _inorderWalk(node: Node<T>): T[] {
    if (!node) {
      return [];
    }
    return [...this._inorderWalk(node.left), node.key, ...this._inorderWalk(node.right)];
  }

  private _postorderWalk(node: Node<T>): T[] {
    if (!node) {
      return [];
    }
    return [...this._postorderWalk(node.left), ...this._postorderWalk(node.right), node.key];
  }
}
